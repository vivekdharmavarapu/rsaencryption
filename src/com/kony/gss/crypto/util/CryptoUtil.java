package com.kony.gss.crypto.util;


import java.security.KeyFactory;
import java.security.PrivateKey;
import java.security.Security;
import java.security.spec.PKCS8EncodedKeySpec;

import javax.crypto.Cipher;

import org.apache.commons.codec.binary.Base64;
import org.apache.log4j.Logger;
import org.bouncycastle.jce.provider.BouncyCastleProvider;



public class CryptoUtil {

	private static Logger LOG = Logger.getLogger(CryptoUtil.class);
	
	static {
		// adds the Bouncy castle provider to java security
		Security.addProvider(new BouncyCastleProvider());
	}

	private static final CryptoUtil instance=new CryptoUtil();;
	private PrivateKey privateKey=null;
	
	public static CryptoUtil getInstance() {
		return instance;
	}
	
	private CryptoUtil() {
		loadPrivateKey();
	}
	
	public String decrypt(String message) {
		return decrypt(message, privateKey);
	}
	
	public String decrypt(String message, PrivateKey privateKey) {
		String decryptedMessage = null;
		try {
			if(LOG.isDebugEnabled()) {
				LOG.debug("Entering decrypt()");
			}
			//Cipher cipher = Cipher.getInstance("RSA/None/NoPadding");
			//Cipher cipher = Cipher.getInstance("RSA/None/NoPadding");
			Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding"); 
			
			cipher.init(Cipher.DECRYPT_MODE, privateKey);
			byte[] messageDecoded = Base64.decodeBase64(message);
			byte[] y = cipher.doFinal(messageDecoded);
			decryptedMessage = new String(y);
			/*if(LOG.isDebugEnabled()) {
				//LOG.debug("Exiting decrypt()");
			}*/

		} catch (Exception exception) {
			exception.printStackTrace();
		}
		if(LOG.isDebugEnabled()) {
			////LOG.debug("decryptedMessage: "+decryptedMessage);
			LOG.debug("Exiting decrypt()");
		}
		return decryptedMessage;
	}

	public void loadPrivateKey() {

		LOG.debug("Entering loadPrivateKey()");
		String pkeyStr  = "MIIJQQIBADANBgkqhkiG9w0BAQEFAASCCSswggknAgEAAoICAQDNKX0E7QNPBOe+" + 
				"6w8fHBDbCyLw7BzwFemGGAUh9+WMzwmOsg28HrT5WyDCLF7S4pxUZmpJFi5UzgzM" + 
				"uta6d0RLoPycCEPwfMC+8gnnexAq87bN6pF3J8C9V1ahsrZAYlot7SS5j+zPAodx" + 
				"PWPV0nyTirOu0+fG6FK3UxPq6fKH5reLCDW7aFuh6TOOejHZQLTDXAziRaBCkpoF" + 
				"4+8OtsjChxOOveBpEMTjJGDhpxPK3XQd2XI5Sgu8v7AzbE7XmaPXmz0VkKB3nul+" + 
				"R4HW+DH11BesZm9J8r/0Ds+/UFHNYrsarisJj9zCxMV67YhD70SJqeBkTy36neLZ" + 
				"B9WlwNSrgi0ceEjrM1YEwKQD84NjgASpwn7ewoHbLnWtjJjPTij5rmt9UycAVBjg" + 
				"sqbtaL3qRT/bwR1qiG1TA8lOkKBtj2EEsvLhN7BYn3OoKkFH/Svm80iKKqtdKeTG" + 
				"OyNWms6Pi7hfa+D1ynuxvxBI8JNPj8UEH3I9UyCCVH+quBfKnIpKHBPU17O9n0Re" + 
				"YFJH+VYeKZvJEYwcuXaYlNA4X8Uev5+mChECjmjEkgkAuu9kn9E1A5ZYO6cOZPX/" + 
				"RYX9K5yNDgNd/EIJ+Q49owPmR86XlgOqv9mNBEMzmhZvRQ35LQHMPND72olpk8fa" + 
				"wAbqw3YM6L/lln/wn9D1CajHwDumVwIDAQABAoICAHNLo4SHJdrcaNg0BRV4kkhm" + 
				"48X9Cu4pgkjnassCIzCUkUxt56YWo6GWgrxcbxWYYD5WJ9jW7EagXTUYst+rcugF" + 
				"mrCdPIEDutYT81jxIc7hk5P5BLKv1nMRiA/q/N0F6KPypzbZGT3kr/UFiPt7BKNt" + 
				"yTiwwYpYuVNpDsJvouqY0AEEJh4hsY02HtEs/qKW6H75Vgw7EKsY9BHnXEsqkXww" + 
				"qN8I+fpRgDQeF4v4dvkEpuK3dqeXYy05lG3xqgUjxJ3AEf3yfxt6ZMGgsej7Jih+" + 
				"fqdWEai/EPy47isXmkEQARInWuxOusOCDXDd2DxFVIEAxKfaj35OCFPrQTebPGf8" + 
				"J9j64jlWfCbgzdf6WH+GHFATs7ZuZ9XLlg5/dQU0mv7eI9X6F/TMkJPGC0r2ZSvq" + 
				"j19hBQVivRO/vmx0L25eI7XGK+XHdm+2lQ+6jQZjXgQtKCNyuLb0hwuYakPl4B8z" + 
				"PlfQfdl1nS5ZpzJsUKc6wf0vxWLyDeVre/+O3yl+MILkLaD3HT8Ln4pwfrIDoziD" + 
				"DUjhfUSzDPdP4dOFTEPt0kMskmEzdD2VOr0nxKsEMlWgxS8FtVPFG2Dp0rKJku+w" + 
				"qCjWHZTSY96iXspNk/KVWFG9T1oiqNYLVKL0sQdoIatYXF6F/UllbTAjlwDUsf1K" + 
				"+M+bLE26oPohMSjHr2+hAoIBAQDwfIrni1vT7vr1gcWkpniju3lptZ8S34LP9iOi" + 
				"heDGeBCEVYjEtZM52CgA5NrvTydEbsJxMWKhegqqmvRdg1yap0grpBT9MiIFKaIg" + 
				"bHM7B+JoFPPQeF2gnOPOpSr6QSC/S5kB5b/c1vVUJVhXvpmDbtT9cNOgoProrl3l" + 
				"MxW3am8q/mpCiNEfxAaZJWEwGWj7UbZFSRRA4XIPDEqhZfGrM/ltIcSaDg6rrfOr" + 
				"u4a8Syv0NZCwaRVfDUt1g3nSXvktL0jNmVrAJn0ScJhPdTazY6YS1pgNbBnbhDe9" + 
				"QfR0tKPJrPEk/NGHfj202gjYOrhKi4gUl32Ig7r42MWZ5nS/AoIBAQDaZZa7U7Jt" + 
				"h7nBM/LgRYPJ+n1uhBY6+Bp7lOQjzBWDVqMCK0ebEPhL+DxPus74RVw6zkb6AbNo" + 
				"wuMqf3eqxDBSOld94n1pBUKe3w/JRD8sWNHSeTBgl8YgoPOt+IuFKnyimQtuIFer" + 
				"5doT/lUKTZb0Q6rcFv9MJd3aNxE4m3Ernrb5JJ3qokhlbnqPUwfFxFwOGCqwhuLS" + 
				"rpL93kM+yIrjTfWa6HIWStPcmCyVvp1A8pTHSsK2vPRgfAvmOjqHkI/kKniz49f3" + 
				"6PRcYORs/yQucFBfiytx58OJ66+zlmKMZG7gKPT243hRwBY7vW33gFMm0OFvTuKP" + 
				"9q7zOIH75zxpAoIBAEPUjQVUHFmBvY1DxjBzt/FOrqcvUCftnSL1JQXxRv4T3WzG" + 
				"B2acZGNbYaUm5826ALRfnT56FHzBRILsY+KAa70/U67jf/wOiOP140HL9RCAX87F" + 
				"oiN538aumd6uXZnqhscOFqEfYWdzxgyu3+UX4Ire1vqJOVfEMCEzFU7Rgt2SCrsQ" + 
				"uX38wyd3jfjiPzBm6mKAoK2YODs4m4g9CkvNykVkyoSKkr51GpQ341Pb99viRAqE" + 
				"lUP8z8dYo6EIrXK10Qh6nslG6P7rnDl4Qq2evQgKIkyL9KVMiVqOR359POsyDESV" + 
				"IAszuBHEfLknCnmazmJQStisBf9cUH9h/RI0m1cCggEAb3dUJE4GjmKsixBhkNfJ" + 
				"+MeRR1vBIN3KXnLSsfaSsFMCpbPDO0DBCb0rwyG8SvjTpuvrS+ScVgbJ/MHMKOsz" + 
				"InTGfOihS320z/NXUkkz2QWIKd1StKoiJDshxBRL2W6i6DYmoFF/jaulz3iKnNSX" + 
				"DBqRVP2j1ZCqF5rtfi/P5gx+pNBsjPnA64wkn4woNmcO+N6awzhFAnS5bzsGpT+j" + 
				"q3n5tdOXjwsn8Ln2X4NHLYPiLYlZsfzk2W3WgO4NO22sEy8eJepPKyfJuHB3ha3/" + 
				"IHiQfQNB7EP3CCizp3xE1hTYhi5BQNU+brMUZe879guX5QLNXIstlX65MkgQB6qw" + 
				"qQKCAQBN0P0FRdY8TvnF7hdky1TYkjjTdIrzT+0TSmouYmrwYFxJ5NMPp5LuN4j/" + 
				"oMuviw653yvKCpxsTmF1eNzvabT4ZoV69Y6lps0mET7sGLcwi2XB1EENmjwo9HWE" + 
				"qd6G0JKK/WSm1pxTLndcWJvyKy91N6b6F17BmXjwNDQO6AiFgdkV6QAvIvwcpYWi" + 
				"0z41EXHlMQeyL6r35PoPL7V8CDGSVh1g6kdXzMyisObgCN2NS9AH+qAQXcSucPYj" + 
				"TOSw3LHU8hKXj04LZwVgUNTesI+kAat5iqOEcMnzMwnY1CKZ0kGlwYyAuE0NiXJN" + 
				"kby68h8opDhrn5HC88CwLDMM8YHO";
		privateKey = generatePrivateKey(pkeyStr);
		LOG.debug("Exiting loadPrivateKey()");
	}

	private PrivateKey generatePrivateKey(String encodedKey) {
		LOG.info("Entering generatePrivateKey()");
		PrivateKey privateKey = null;
		try {

			byte[] privateKeyDecoded = Base64.decodeBase64(encodedKey);
			PKCS8EncodedKeySpec pkcs8EncodedKeySpec = new PKCS8EncodedKeySpec(
					privateKeyDecoded);
			KeyFactory factory = KeyFactory.getInstance("RSA", "BC");
			privateKey = factory.generatePrivate(pkcs8EncodedKeySpec);

		} catch (Exception exception) {
			LOG.error("Error generatePrivateKey", exception);
		}
		LOG.info("Exiting generatePrivateKey()");
		return privateKey;
	}

}
